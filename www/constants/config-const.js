'use strict';
angular.module('main')
.constant('Config', {

  // gulp environment: injects environment vars
  ENV: {
    /*inject-env*/
    'SERVER_URL': 'http://localhost:3000/api',
    'RESOURCES_URL': 'http://localhost:3000'
    /*'SERVER_URL': 'http://musicadvisor-89764.onmodulus.net/api',
    'RESOURCES_URL': 'http://musicadvisor-89764.onmodulus.net/'*/
    /*endinject*/
  },

  // gulp build-vars: injects build vars
  BUILD: {
    /*inject-build*/
    /*endinject*/
  }

});
