'use strict';
(function(angular) {
  angular.module('main')
    .factory('ArtistService', function($http, Config) {
      var baseUrl = Config.ENV.SERVER_URL + '/artist';

      return {
        getInfo: function(id) {
          return $http.get(baseUrl + '/' + id);
        }
      };

    });
})(angular);
