'use strict';

angular.module('main')
  .factory('SearchService', function($log, $http, $q, Config) {

    var baseUrl = Config.ENV.SERVER_URL + '/search';
    return {

      byString: function(string) {
        var deferred = $q.defer();
        var completion = {
          fast: false,
          deep: false
        };

        var callback = function() {
          if (completion.fast && completion.deep) {
            deferred.resolve();
          }
        };

        $http.get(baseUrl + '/fast?string=' + string)
          .success(function(res) {
            completion.fast = true;
            deferred.notify(res);
            callback();
          })
          .error(function(res) {
            deferred.reject(res);
          });

        $http.get(baseUrl + '/deep?string=' + string)
          .success(function(res) {
            completion.deep = true;
            deferred.notify(res);
            callback();
          })
          .error(function(res) {
            deferred.reject(res);
          });

        return deferred.promise;
      }

    };

  });
