'use strict';
(function (angular) {

    angular.module('main')
        .factory('AuthService', function ($http, $log, $state, $q, $timeout, Config, SessionService, $localStorage) {

            /*return $resource(
              'http://localhost:3000/api/user/:action',
              {},
              {
                'login': {
                  method: 'POST',
                  params: { action: 'login' }
                }
              }
            );*/

            return {
                login: function (loginData, callback) {
                    $http.post(Config.ENV.SERVER_URL + '/user/login', loginData)
                        .then(
                            function (res) {
                                SessionService.isLoggedIn = true;
                                SessionService.token = res.data.token;
                                $localStorage.set("x-access-token", res.data.token);
                                console.log("[jay] Questo è il session token : " + res.data.token);
                                console.log("[jay] Questo è il session token preso dal $localstorage : " + $localStorage.get("x-access-token"));
                                callback(res);
                            },

                            function () {
                                callback(false);
                            }
                        );
                },

                logout: function () {
                    SessionService.isLoggedIn = false;
                    SessionService.token = '';
                    $localStorage.set("x-access-token", undefined);
                },

                /*
                  data: {
                    username: 'username',
                    password: 'password',
                    displayName: 'displayName',
                    birthDate: Date,
                    superEmail: 'email supervisore',
                    superPwd: 'password supervisore'
                  }
                */
                register: function (data) {
                    return $http.post(Config.ENV.SERVER_URL + '/user/register', data);
                },

                authorize: function () {
                    var deferred = $q.defer();

                    $timeout(function () {
                        if (!SessionService.isLoggedIn) {
                            $state.go('initial');
                            deferred.reject();
                        } else {
                            deferred.resolve();
                        }
                    });
                    return deferred.promise;
                }
            };

        });

})(angular);
