'use strict';
angular.module('main')
.factory('FriendService', function($log, $http, Config) {
  var baseUrl = Config.ENV.SERVER_URL + '/friends';

  return {

    addFriend: function(id) {
      return $http.post(baseUrl + '/' + id);
    },

    removeFriend: function(id) {
      return $http.post(baseUrl + '/' + id + '/remove');
    },

    getFriends: function() {
      return $http.get(baseUrl + '/');
    },

    // synchronous method to be inserted in async context
    getRequests: function() {
      return $http.get(baseUrl + '/requests');
    },

    acceptRequest: function(id) {
      return $http.post(baseUrl + '/request/' + id + '/accept');
    },

    refuseRequest: function(id) {
      return $http.post(baseUrl + '/request/' + id + '/refuse');
    },

    // mark friend requests as read
    readRequests: function() {
      return $http.post(baseUrl + '/requests/read');
    },

    withdrawRequest: function(id) {
      return $http.post(baseUrl + '/request/' + id + '/withdraw');
    }

  };
});
