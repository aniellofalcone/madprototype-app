'use strict';
angular.module('main')
.service('NotificationsService', function($log, $http, Config, FriendService,$q,lodash) {
  var baseUrl = Config.ENV.SERVER_URL + '/friends';

    var deferred = $q.defer();
    var self = this;

    this.getNotifications = function(){
      FriendService.getRequests().then(function(res){
        self.notifications = 0;
        lodash.forEach(res.data.data,function(item){
          self.notifications += (item.toRead == true) ? 1 : 0;
        })
        deferred.resolve(self.notifications);
      },function(err){
        deferred.reject();
      });

      return deferred.promise;
    }

  
});
