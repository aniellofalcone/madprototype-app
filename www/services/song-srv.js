'use strict';
(function(angular) {

  angular.module('main')
    .factory('SongService', function($log, $http, Config) {
      var baseUrl = Config.ENV.SERVER_URL + '/song/';

      return {

        getSong: function(id) {
          return $http.get(baseUrl + id);
        },

        getRating: function(id) {
          return $http.get(baseUrl + id + '/rating');
        },

        setRating: function(id, rating) {
          return $http.put(baseUrl + id + '/rating', rating);
        },

        getTags: function(id) {
          return $http.get(baseUrl + id + '/tags');
        },

        setTags: function(id, tags) {
          return $http.put(baseUrl + id + '/tags', tags);
        },

        getComments: function(id) {
          return $http.get(baseUrl + id + '/comments');
        },

        addComment: function(id, comment) {
          return $http.post(baseUrl + id + '/comment', comment);
        },

        editComment: function(id, comment) {
          return $http.post(baseUrl + id + '/comment/' + comment._id, comment);
        },

        deleteComment: function(id, comment) {
          return $http.post(baseUrl + id + '/comment/' + comment._id + '/delete');
        }

      };
    });

})(angular);
