'use strict';
(function (angular) {
    //INTERCEPTORS FACTORY FOR AUTOLOGIN-LOGOUT + TOKEN
    angular.module('main')
        .factory('TokenInterceptor', function ($log, Config, SessionService, $localStorage, $q) {
            var tokenInjector = {
                request: function (config) {
                    var localToken = $localStorage.get("x-access-token");
                    if (SessionService.isLoggedIn === true) {
                        config.headers['x-access-token'] = SessionService.token;
                    } else if (localToken !== undefined && localToken !== 'undefined') { // Auto login
                        SessionService.isLoggedIn === true
                        SessionService.token = localToken;
                        config.headers['x-access-token'] = localToken
                    }
                    return config;
                },
                responseError: function (response) {
                    if (response.status === 401) {
                        $log.log(response, ' redirect auth');
                        SessionService.isLoggedIn === false
                        SessionService.token = "";
                        config.headers['x-access-token'] = undefined;
                        $state.go('signin');
                    }
                    if (response.status === 503) {
                        alert("Server momentaneamente non disponibile");
                    }
                    return $q.reject(response);
                }
            };
            return tokenInjector;
        })
        .config(function ($httpProvider) {
            $httpProvider.interceptors.push('TokenInterceptor');
        });
})(angular);
