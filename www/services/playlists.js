'use strict';
(function(angular) {
  angular.module('main')
    .factory('PlaylistService', function($log, $http, Config) {
      var baseUrl = Config.ENV.SERVER_URL + '/playlist/';

      return {

        getPlaylists: function(userId) {
          var url = baseUrl+"user/";
          if(userId){
            url +=  userId;
          }

          return $http.get(url);
        },

        getPlaylistInfo: function(playlistId) {
          return $http.get(baseUrl + playlistId);
        },

        addPlaylist: function(playlist){
            // playlist = {
            //   title:'',
            //   description:'',
            //   imageUrl:''
            // };
          var url = baseUrl+"new";
          return $http.post(url,playlist);
        },

        updatePlaylist: function(playlistId, playlist){
          var url = baseUrl+playlistId+"/edit";
          return $http.post(url);
        },

        addSongToPlaylist: function(songId,playlistId){
          var url = baseUrl+playlistId+"/song";
          return $http.post(url,{action:'add',id:songId});
        },

        deleteSongFromPlaylist: function(songId,playlistId){
          var url = baseUrl+playlistId+"/song";
          return $http.post(url,{action:'remove',id:songId});
        },

        deletePlaylist: function(playlistId) {
          var url = baseUrl+playlistId+"/delete";
          return $http.post(url);
        }

      };
    });

})(angular);
