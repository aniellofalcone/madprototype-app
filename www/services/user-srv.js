'use strict';

(function(angular) {

  angular.module('main')
    .factory('UserService', function($http, Config) {
      var baseUrl = Config.ENV.SERVER_URL + '/user';

      return {

        getAvatar: function(id) {
          if (id) {
            return $http.get(baseUrl + '/' + id + '/avatar');
          } else {
            return $http.get(baseUrl + '/avatar');
          }
        },

        getInfo: function(id) {
          if (id) {
            return $http.get(baseUrl + '/' + id);
          } else {
            return $http.get(baseUrl);
          }
        },

        updateInfo: function(info) {
          return $http.post(baseUrl + '/', info);
        },

        updateAvatar: function(avatar) {
          return $http.post(baseUrl + '/avatar', {avatar: avatar});
        },

        delete: function() {
          return $http.post(baseUrl + '/delete');
        },

        getMyLastThreeSongs: function(){
          return $http.get(baseUrl + '/latest-songs');
        },

        getUserLastThreeSongs: function(id){
          return $http.get(baseUrl + '/'+id+'/latest-songs');
        }

      };
    });

})(angular);
