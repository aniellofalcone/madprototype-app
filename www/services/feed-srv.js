'use strict';
angular.module('main')
  .factory('FeedService', function($log, $http, Config, $q) {
    return {

      // get a news feed of 'limit'-many events starting from 'skip'
      getFeed: function(skip, limit) {
        limit = limit || 20;
        skip = skip || 0;

        var url = Config.ENV.SERVER_URL + '/feed';
        url += '?limit=' + limit;
        url += '&skip=' + skip;

        var defer = $q.defer();
        $http.get(url).success(function(data) {
          defer.resolve(data);
        }).error(function(err) {
          console.log(err);
          defer.reject();
        });
        return defer.promise;

        //return $http.get(url);
      }

    };
  });
