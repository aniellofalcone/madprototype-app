'use strict';
(function(angular) {

  angular.module('main')
    .service('SessionService', function() {
      this.isLoggedIn = false;
      this.token = '';

    });

})(angular);
