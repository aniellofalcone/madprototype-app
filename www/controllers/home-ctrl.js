'use strict';
(function (angular) {
  angular.module('main')
    .controller('HomeCtrl', function ($log, $scope, $state, $stateParams,FeedService,lodash) {
      $log.log('hello from HomeCtrl!!');

      $scope.showSpinner = true;
      $scope.datas = [];
      FeedService.getFeed().then(function(datas){
        $scope.showSpinner = false;
        console.log(datas);
        $scope.datas = datas;
        $scope.datas.push(datas);
      },function(err){
        console.log("error: ",err);
      });

    });
})(angular);
