'use strict';
angular.module('main')
.controller('NotificationsCtrl', function($log, $scope, $ionicPopup, FriendService, lodash, UserService, $q) {
  //$scope.checkNotifications();

  $scope.notifications = [];
  $scope.showSpinner = true;
  FriendService.getRequests().then(function(res){

    
    $scope.notifications = res.data.data;

    if(res.data.data && res.data.data.length>0){
      $scope.showSpinner = false;
      getAllAvatars(res.data.data).then(function(avatars){
        console.log(avatars);

        for(var i=0; i<$scope.notifications.length; i++){
          $scope.notifications[i].actor.avatarUrl = avatars[i].data;
        }
      })
    }else{
      $scope.showMessage = true;
      $scope.showSpinner = false;
    }

    
    
  })

  function getAllAvatars(arr){
    var promises = [];
    var deferred = $q.defer();

    lodash.forEach(arr,function(item){
      promises.push(UserService.getAvatar(item.actor._id));
    });

    $q.all(promises).then(function(data){
      deferred.resolve(data);
    },function(err){
      deferred.reject(err);
    });

    return deferred.promise;
  }

  $scope.accept = function(index,id) {
    FriendService.acceptRequest(id).success(function() {
      $ionicPopup.show({
        template: '<p>Richiesta accettata!</p>',
        title: 'Successo',
        buttons: [{ text: 'Ok' }]
      });
      //$scope.checkNotifications();
      $scope.notifications.splice(index, 1);
      if($scope.notifications.length==0){
        $scope.showMessage = true;
      }
    });
  };

  $scope.ignore = function(index,id) {
    FriendService.refuseRequest(id).success(function() {
      //$scope.checkNotifications();
      $scope.notifications.splice(index, 1);
      if($scope.notifications.length==0){
        $scope.showMessage = true;
      }
    });
  };
});
