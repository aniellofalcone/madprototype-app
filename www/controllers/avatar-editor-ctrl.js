'use strict';

(function(angular) {

  angular.module('main')
    .controller('AvatarEditorCtrl', function($rootScope, $scope, $ionicSideMenuDelegate) {

      $scope.$on('$ionicView.enter', function() {
        $ionicSideMenuDelegate.canDragContent(false);
      });
      $scope.$on('$ionicView.leave', function() {
        $ionicSideMenuDelegate.canDragContent(true);
      });

    });

})(angular);
