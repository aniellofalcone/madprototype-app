'use strict';
(function(angular) {
  angular.module('main')
    .controller('ArtistDetailCtrl', function($stateParams, $scope, ArtistService) {
      ArtistService.getInfo($stateParams.id)
        .success(function(res) {
          $scope.artist = res.data;
        });
    });
})(angular);
