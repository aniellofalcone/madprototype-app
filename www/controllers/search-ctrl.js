'use strict';
angular.module('main')
  .controller('SearchCtrl', function($log, $scope, SearchService, Config, $state) {
    $scope.query = {};

    $scope.goTo = function(id){
      var params = {
        id:id
      }
      $state.go('app.artistDetail',params);
    }

    $scope.submit = function() {
      $scope.isSearching = true;
      $scope.results = {
        users: [],
        songs: [],
        artists: [],
        playlists: []
      };

      SearchService.byString($scope.query.string).then(
        function() {
          $scope.isSearching = false;
        },
        function(res) {
          $scope.isSearching = false;
          $log.log(res);
        },
        function(res) {
          for (var i = 0; i < res.users.length; i++) {
            res.users[i].avatarUrl =
              Config.ENV.RESOURCES_URL + '/avatars/' +
              res.users[i].avatarId;
          }

          $scope.results = {
            users: $scope.results.users.concat(res.users),
            songs: $scope.results.songs.concat(res.songs),
            playlists: $scope.results.playlists.concat(res.playlists),
            artists: $scope.results.artists.concat(res.artists)
          };

          $scope.labels = {
            users: ($scope.results.users.length > 0),
            songs: ($scope.results.songs.length > 0),
            playlists: ($scope.results.playlists.length > 0),
            artists: ($scope.results.artists.length > 0)
          };
        }
      );
    };
  });
