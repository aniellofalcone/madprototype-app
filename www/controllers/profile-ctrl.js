'use strict';
angular.module('main')
.controller('ProfileCtrl', function($log, $scope, $sce, $stateParams, $state,
                                    $ionicPopup, UserService, FriendService, $ionicLoading) {

  $scope.avatar = {
    src: null,
    show: false
  };

  $scope.userId = $stateParams.id;


  $scope.show = function() {
      $ionicLoading.show({
        template: 'Loading...'
      }).then(function(){
         console.log("The loading indicator is now displayed");
      });
    };

  $scope.hide = function(){
    $ionicLoading.hide().then(function(){
       console.log("The loading indicator is now hidden");
    });
  };

  $scope.goToSongDetail = function(id){
    $state.go('app.songDetail',{id:id});
  }

  $scope.show();
  // if the profile is mine
  if (!$scope.userId) {
    $scope.isThisUser = true;

    UserService.getAvatar().success(function(res) {
      $scope.avatar.src = res;
      $scope.avatar.show = true;

      UserService.getInfo().success(function(res) {
        $scope.user = res.data;

        $scope.newUserProfile = {
          oldPassword:'',
          newPassword:'',
          displayName:$scope.displayName
        }

        $scope.hide();

        UserService.getMyLastThreeSongs().then(function(res){
          console.log('MyLastThreeSongs',res.data);

          $scope.lastSongs = res.data;
        })

      });

    });

    

  //if the profile someone else's
  } else {
    $scope.isThisUser = false;

    UserService.getAvatar($scope.userId).success(function(res) {
      $scope.avatar.src = res;
      $scope.avatar.show = true;

      UserService.getInfo($scope.userId).success(function(res) {
        $scope.user = res.data;
        $scope.hide();
      });

      UserService.getUserLastThreeSongs($scope.userId).then(function(res){
          console.log('UserLastThreeSongs',res.data);
          $scope.lastSongs = res.data;
        })

    });

    
  }

  $scope.showAlert = function(title,message) {
     var alertPopup = $ionicPopup.alert({
       title: title,
       template: message,
       buttons: [{text: '<b>OK</b>',type: 'button-assertive'}]
     });

     alertPopup.then(function(res) {
       //console.log('Thank you for not eating my delicious ice cream cone');
     });
  };


  $scope.saveProfile = function(newUserProfile){
    $scope.show();
    UserService.updateInfo(newUserProfile).then(function(res){
      $scope.user = res.data.data;
      $scope.hide();
      $scope.showAlert('Profilo aggiornato','Congratulazioni, il tuo profilo è stato aggiornato correttamente!');
    },function(err){
      $scope.errors = {}
      $scope.hide();
      console.log('profile update error: ',err);
      if(err.status == 401){
        $scope.showAlert('Password errata','La vecchia password non corrisponde a quella inserita!');
      }
    })
  }

  $scope.addFriend = function() {
    FriendService.addFriend($scope.userId).success(function(res) {
      $ionicPopup.show({
        template: '<p>Richiesta inviata!</p>',
        title: 'Successo',
        buttons: [{ text: 'Ok'}]
      });
      $scope.user.requestId = res.requestId;

    }).error(function(res) {
      $ionicPopup.show({
        template: '<p>' + res.message + '</p>',
        title: 'Errore',
        buttons: [{ text: 'Ok' }]
      });

    });
  };

  $scope.removeFriend = function() {
    $ionicPopup.show({
      template: '<p>Vuoi davvero rimuovere ' +
        $scope.user.displayName + ' come amico/a?',
      title: 'Conferma rimozione',
      scope: $scope,
      buttons: [
        { text: 'No' },
        { text: '<b>Sì</b>', onTap: function() {
          FriendService.removeFriend($scope.userId).success(function() {

            $scope.user.friends = false;

          }).error(function(res) {
            $ionicPopup.show({
              template: '<p>' + res.message + '</p>',
              title: 'Errore',
              buttons: [{ text: 'Ok' }]
            });
          });
        }}
      ]
    });
  };

  $scope.withdrawRequest = function() {
    $ionicPopup.show({
      template: '<p>Vuoi davvero annullare la richiesta di amicizia?</p>',
      title: 'Conferma',
      scope: $scope,
      buttons: [
        { text: 'No' },
        { text: '<b>Sì</b>', onTap: function() {
          FriendService.withdrawRequest($scope.user.requestId).success(function() {
            $ionicPopup.show({
              template: '<p>Richiesta annullata.</p>',
              title: 'Successo',
              buttons: [{ text: 'Ok' }]
            });
            $scope.user.requestId = false;

          }).error(function(res) {
            $ionicPopup.show({
              template: '<p>' + res.message + '</p>',
              title: 'Errore',
              buttons: [{ text: 'Ok' }]
            });

          });
        }}
      ]
    });
  };

  $scope.editAvatar = function() {
    $state.go('app.avatar');
  };
});
