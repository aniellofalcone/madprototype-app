'use strict';
(function(angular) {
  angular.module('main')
    .controller('FriendsCtrl', function($log, $scope, $q, $ionicPopup,
                                        FriendService, UserService,
                                        lodash) {
      $scope.friends = null;
      $scope.showMessage = false;

      FriendService.getFriends()
        .success(function(res) {
          $scope.friends = res.data;
          if($scope.friends.length==0){
            $scope.showMessage = true;
          }else{
            getAvatars();
            $scope.showMessage = false;
          }
          
        })
        .error(function(res) {
          $ionicPopup.show({
            template: '<p>' + res.message + '</p>',
            title: 'Error ',
            buttons: [{ text: 'Ok' }]
          });
        });

      function getAvatars() {
        lodash.forEach($scope.friends, function(friend, i) {
          UserService.getAvatar(friend._id).success(function(res) {
            $scope.friends[i].avatarSrc = res;
          });
        });
      }
    });
})(angular);
