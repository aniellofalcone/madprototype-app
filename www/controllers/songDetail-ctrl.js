'use strict';
(function(angular) {
  angular.module('main')
    .controller('SongDetailCtrl', function($scope, $rootScope, $ionicModal, $stateParams, $log, $sce, UserService, lodash, SongService, Config, $ionicPopup, $ionicLoading, PlaylistService) {
      $log.log('welcome from songDetailCtrl', $stateParams);

      $rootScope.$on('commentAdded', function(event, args) {

      });

      SongService.getSong($stateParams.id);

      SongService.getTags($stateParams.id).then(function(res) {
        $log.log('getTags', res.data.data);
        $scope.tags = res.data.data;
      });

      $scope.showAlert = function(title, message) {
        var alertPopup = $ionicPopup.alert({
          title: title,
          template: message,
          buttons: [{
            text: '<b>OK</b>',
            type: 'button-assertive'
          }]
        });

        alertPopup.then(function() {
          //console.log('Thank you for not eating my delicious ice cream cone');
        });
      };

      $scope.show = function() {
        $ionicLoading.show({
          template: 'Loading...'
        }).then(function() {
          $log.log('The loading indicator is now displayed');
        });
      };

      $scope.hide = function() {
        $ionicLoading.hide().then(function() {
          $log.log('The loading indicator is now hidden');
        });
      };

      $scope.tags = {
        happy_01: {
          selected: false,
          qty: 0
        },
        happy_02: {
          selected: false,
          qty: 0
        },
        love_01: {
          selected: false,
          qty: 0
        },
        love_02: {
          selected: false,
          qty: 0
        },
        sad_01: {
          selected: false,
          qty: 0
        },
        sad_02: {
          selected: false,
          qty: 0
        },
        sad_03: {
          selected: false,
          qty: 0
        },
        boring_01: {
          selected: false,
          qty: 0
        }
      };

      $scope.musicRating = {};
      $scope.videoRating = {};
      $scope.lyricsRating = {};

      SongService.getRating($stateParams.id).then(function(res) {
        $scope.musicRating.rate = res.data.data.avg.music;
        $scope.videoRating.rate = res.data.data.avg.video;
        $scope.lyricsRating.rate = res.data.data.avg.lyrics;

        $scope.musicRating.myRate = res.data.data.myRate.music / 20;
        $scope.videoRating.myRate = res.data.data.myRate.video / 20;
        $scope.lyricsRating.myRate = res.data.data.myRate.lyrics / 20;
      });

      $scope.musicRating.max = 5;
      $scope.videoRating.max = 5;
      $scope.lyricsRating.max = 5;

      $scope.videoUrl = $sce.trustAsResourceUrl(Config.ENV.SERVER_URL + '/song/' + $stateParams.id + '/video');


      $scope.rate = function() {
        $scope.show();
        var rate = {
          music: $scope.musicRating.myRate * 20,
          video: $scope.videoRating.myRate * 20,
          lyrics: $scope.lyricsRating.myRate * 20
        };


        SongService.setRating($stateParams.id, rate).then(function(res) {
          $scope.hide();

          $scope.showAlert('Hai votato!', 'Congratulazioni, il tuo voto è stato registrato!');

          $scope.musicRating.rate = res.data.data.music / 20;
          $scope.videoRating.rate = res.data.data.video / 20;
          $scope.lyricsRating.rate = res.data.data.lyrics / 20;
        });
      };

      $scope.tag = function(key) {
        $log.log('key', key);

        $scope.tags[key].selected = !$scope.tags[key].selected;

        if ($scope.tags[key].selected) {
          $scope.tags[key].qty += 1;
        } else {
          $scope.tags[key].qty -= 1;
        }

        $log.log('$scope.tags', $scope.tags);

        SongService.setTags($stateParams.id, $scope.tags);
      };

      function refreshComments() {
        SongService.getComments($stateParams.id)
          .success(function(res) {

            $scope.comments = res.data;
            lodash.forEach($scope.comments, function(comment) {
              UserService.getAvatar(comment.owner.id)
                .success(function(res) {
                  comment.owner.avatar = res;
                });
            });
          });
      }
      refreshComments();

      //PLAYLIST

      $scope.playlists = [{
        title: 'Reggae',
        id: 1
      }, {
        title: 'Chill',
        id: 2
      }, {
        title: 'Dubstep',
        id: 3
      }, {
        title: 'Indie',
        id: 4
      }, {
        title: 'Rap',
        id: 5
      }, {
        title: 'Cowbell',
        id: 6
      }];

      $ionicModal.fromTemplateUrl('templates/playlist.html', {
        scope: $scope,
        animation: 'slide-in-up'
      }).then(function(modal) {
        $scope.playlistModal = modal;
      });

      $scope.openPlaylistModal = function() {
        PlaylistService.getPlaylists().then(function(data) {
          $log.log(data);
          $scope.playlistModal.show();
        }, function(err) {
          $log.log(err);
        });
        $scope.playlistModal.show();
      };

      $scope.closePlaylistModal = function() {
        $scope.playlistModal.hide();
      };

      $ionicModal.fromTemplateUrl('templates/create-playlist.html', {
        scope: $scope,
        animation: 'slide-in-up'
      }).then(function(modal) {
        $scope.createPlaylistModal = modal;
      });

      $scope.openCreatePlaylistModal = function() {
        $scope.createPlaylistModal.show();
      };

      $scope.closeCreatePlaylistModal = function() {
        $log.log("Exiting from playlistModal");
        $scope.createPlaylistModal.hide();
      };

      $scope.playlist = {
        title: '',
        description: '',
        imageUrl: ''
      };

      $scope.plId = 10;

      $scope.songList = [{
        title: "One Love",
        artist: "Bob Marley"
      }, {
        title: "Could you be loved",
        artist: "Bob Marley"
      }];

      // CREATE PLAYLIST FUNCTION
      $scope.createPlaylist = function() {
        PlaylistService.addPlaylist()
          .then(function(res) {
            $scope.playlist = {
              title: $scope.playlist.title,
              description: $scope.playlist.description,
              imageUrl: "",
              id: $scope.plId
            };
            $log.log("title >>", $scope.playlist.title);
            $log.log("description >>", $scope.playlist.description);
            $scope.plId++;
            $scope.playlists.push($scope.playlist);
            $scope.showCreationAlert("Complimenti!", 'Playlist creata con successo!');
            $scope.playlist = {
              title: '',
              description: '',
              imageUrl: ''
            };
          }, function(err) {
            $log.log(err);
          });
      };

      $scope.showCreationAlert = function(title, message) {
        var creationAlert = $ionicPopup.alert({
          title: title,
          template: message,
          buttons: [{
            text: '<b>OK</b>',
            type: 'button-assertive',
            onTap: function() {
              $scope.closePlaylistModal();
            }
          }]
        });
      };

      // ADDING SONGS TO PLAYLISTS
      $scope.addThisSong = function () {
        PlaylistService.addSongToPlaylist()
        .then(function(res){

        });
      };

      // COMMENTS

      $ionicModal.fromTemplateUrl('templates/comment-modal.html', {
        scope: $scope
      }).then(function(modal) {
        $scope.commentModal = modal;
      });

      $scope.selectedComment = {};

      $scope.openCommentModal = function(comment) {
        if (comment) {
          $scope.selectedComment = comment;
        }
        $scope.commentModal.show();
      };

      $scope.closeCommentModal = function() {
        $scope.selectedComment = {};
        $scope.commentModal.hide();
      };

      $scope.postComment = function() {
        $log.log($scope.selectedComment);
        if ($scope.selectedComment._id) {
          SongService.editComment(
              $stateParams.id,
              $scope.selectedComment

            ).success(function() {
              refreshComments();
              $scope.closeCommentModal();
            })
            .error(function(res) {
              $log.log(res);
            });

        } else {
          SongService.addComment(
              $stateParams.id,
              $scope.selectedComment
            )
            .success(function() {
              refreshComments();
              $scope.closeCommentModal();
            })
            .error(function(res) {
              $log.log(res);
            });
        }
      };
    });
})(angular);
