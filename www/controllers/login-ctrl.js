'use strict';
(function(angular) {
  angular.module('main')
    .controller('loginCtrl', function($log, $scope, $state, $stateParams,
      $ionicModal, $ionicPopup, AuthService,
      ionicDatePicker, UserService, $rootScope, $ionicLoading) {

      $scope.registerData = {};
      $scope.message = $stateParams.message;

      var datePicker = {
        callback: function(val) {
          $scope.registerData.birthDate = new Date(val);
          $scope.formattedBirthDate = $scope.registerData.birthDate.toLocaleDateString();
        }
      };

      $ionicModal.fromTemplateUrl('templates/register-modal.html', {
        scope: $scope
      }).then(function(modal) {
        $scope.registerModal = modal;
      });

      $scope.user = {};

      $scope.show = function() {
        $ionicLoading.show({
          template: 'Loading...'
        }).then(function() {
          console.log("The loading indicator is now displayed");
        });
      };

      $scope.hide = function() {
        $ionicLoading.hide().then(function() {
          console.log("The loading indicator is now hidden");
        });
      };

      $scope.login = function() {
        $scope.show();
        AuthService.login($scope.user, function(success) {
          if (success && success.data.hasAvatar) {
            UserService.getAvatar().then(function(res) {
              $rootScope.base64 = res.data;

              UserService.getInfo().then(function(userInfo) {
                $rootScope.displayName = userInfo.data.data.displayName;
                $scope.hide();
                $state.go('app.home');

              });

            });

          } else if (success && !success.data.hasAvatar) {
            $scope.hide();
            $state.go('app.avatar');
          } else {
            $scope.hide();
            $scope.user.password = '';
            $scope.message = {
              content: 'Username o password errati',
              error: true,
              show: true
            };
          }
        });
      };

      $scope.register = function() {
        AuthService.register($scope.registerData).then(
          function() {
            $scope.closeRegisterModal();
            $scope.message = {
              content: 'Registrazione riuscita',
              info: true,
              show: true
            };
          },
          function(res) {
            $ionicPopup.show({
              template: '<p>' + res.data.message + '</p>',
              title: 'Error',
              buttons: [{
                text: 'Ok'
              }]
            });
          }
        );
      };

      $scope.openRegisterModal = function() {
        $scope.registerModal.show();
      };

      $scope.closeRegisterModal = function() {
        $scope.registerModal.hide();
      };

      $scope.openDatePicker = function() {
        ionicDatePicker.openDatePicker(datePicker);
      };
    });
})(angular);
