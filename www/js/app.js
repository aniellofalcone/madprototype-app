// Ionic music App
'use strict';
// angular.module is a global place for creating, registering and retrieving Angular modules
// 'music' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'music.controllers' is found in controllers.js
angular.module('main', [
  'ionic',
  'main.controllers',
  'ionic-datepicker',
  'ngLodash',
  'ionic.rating'
])

.run(["$ionicPlatform", "$state", "$log", "$localStorage", function ($ionicPlatform, $state, $log, $localStorage) {
    $ionicPlatform.ready(function () {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            cordova.plugins.Keyboard.disableScroll(true);
            //screen.lockOrientation('portrait');
        }
        $log.log(window.StatusBar);
        if (window.StatusBar) {
            return StatusBar.hide();
        }
        var AuthToken = $localStorage.get('x-access-token');
        if (AuthToken !== undefined && AuthToken !== 'undefined') {
            $log.log("go to --> app.home");
            $state.go('app.home');
        } else {
            $log.log("go to --> singin");
            return $state.go('signin');
        }
    });
}])

.config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('signin', {
            url: '/sign-in',
            templateUrl: 'templates/login.html',
            controller: 'loginCtrl'
        })

    .state('forgotpassword', {
        url: '/forgot-password',
        templateUrl: 'templates/forgot-password.html'
    })

    .state('app', {
        url: '/app',
        abstract: true,
        templateUrl: 'templates/menu.html',
        controller: 'AppCtrl',
        cache: false
    })

    .state('app.home', {
        url: '/home',
        views: {
            'menuContent': {
                templateUrl: 'templates/home.html',
                controller: 'HomeCtrl as home'
            }
        }
    })

    .state('app.profile', {
        cache: false,
        url: '/profile/:id',
        views: {
            'menuContent': {
                templateUrl: 'templates/profile.html',
                controller: 'ProfileCtrl'
            }
        }
    })

    .state('app.avatar', {
        cache: false,
        url: '/avatar',
        views: {
            'menuContent': {
                templateUrl: 'templates/avatar-editor.html',
                controller: 'AvatarEditorCtrl'
            }
        }
    })

    .state('app.notifications', {
        url: '/notifications',
        views: {
            'menuContent': {
                templateUrl: 'templates/notifications.html',
                controller: 'NotificationsCtrl'
            }
        }
    })

    .state('app.browse', {
        url: '/friends',
        views: {
            'menuContent': {
                templateUrl: 'templates/friends.html',
                controller: 'FriendsCtrl'
            }
        }
    })

    .state('app.search', {
        url: '/search',
        views: {
            'menuContent': {
                templateUrl: 'templates/search.html',
                controller: 'SearchCtrl'
            }
        }
    })

    .state('app.songDetail', {
        url: '/songDetail/:id',
        views: {
            'menuContent': {
                templateUrl: 'templates/songDetail.html',
                controller: 'SongDetailCtrl'
            }
        }
    })

    .state('app.playlists', {
        url: '/playlists',
        views: {
            'menuContent': {
                templateUrl: 'templates/playlists.html',
                controller: 'SongDetailCtrl'
            }
        }
    })

    .state('app.single', {
        url: '/playlists/:playlistId',
        views: {
            'menuContent': {
                templateUrl: 'templates/playlist.html',
                controller: 'SongDetailCtrl'
            }
        }
    })

    .state('app.artistDetail', {
        url: '/artist/:id',
        views: {
            'menuContent': {
                templateUrl: 'templates/artist-detail.html',
                controller: 'ArtistDetailCtrl'
            }
        }
    });
    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/sign-in');
})

// LOCAL STORAGE FACTORY
.factory('$localStorage', ['$window', function ($window) {
    return {
        set: function (key, value) {
            $window.localStorage[key] = value;
        },
        get: function (key, defaultValue) {
            return $window.localStorage[key] || defaultValue;
        },
        setObject: function (key, value) {
            $window.localStorage[key] = JSON.stringify(value);
        },
        getObject: function (key) {
            console.error("DEPRECATED");
        }
    };
}]);
