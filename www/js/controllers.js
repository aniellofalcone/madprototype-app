'use strict';
(function (angular) {

    angular.module('main.controllers', [])

    .controller('AppCtrl', function ($scope, $localStorage, $ionicModal, $timeout, $state, $interval, AuthService, FriendService, Config, $rootScope, NotificationsService) {

        var notifEnabled = $interval($scope.checkNotifications, 600000);


        $rootScope.$on('avatarUpdated', function (event, args) {

            $scope.$apply(function () {
                $scope.avatarSrc = args;
            })

        })

        $scope.logout = function () {
            AuthService.logout();
            $interval.cancel(notifEnabled);
            if ($location.$path != "/sign-in") {
                $location.path('/sign-in');
            }
            $state.go('signin', {
                message: {
                    content: 'Logout riuscito',
                    info: true,
                    show: true
                }
            });
        };

        $scope.avatarSrc = $rootScope.base64;
        $scope.displayName = $rootScope.displayName;


        NotificationsService.getNotifications().then(function (data) {
            $scope.badgeNumber = data;
        });

        //$scope.avatarSrc = 'data:image/svg+xml;base64,' + btoa($rootScope.base64)


        /* $scope.checkNotifications = function() {
           FriendService.getRequests().success(function(res) {
             if (res.data.length > 0) {
               $scope.notifPresent = true;
             } else {
               $scope.notifPresent = false;
             }
             for (var i = 0; i < res.data.length; i++) {
               res.data[i].actor.avatarUrl =
                 Config.ENV.RESOURCES_URL + '/avatars/' +
                 res.data[i].actor.avatarId;
             }
             $scope.notifications = res.data;

           });
         };
         $scope.checkNotifications(); */

    })

})(angular);
